import matplotlib.pyplot as plt
import numpy as np

from arostest import arostest

np.random.seed(42)  # for reproducibility

n_tests = 10000

all_h = []
for n in range(n_tests):
    data = np.random.random((50, 3))
    all_h.append(arostest(data)['h'])

# load precomputed data
# all_h = np.load('far_data.npy')

plt.plot(np.cumsum(all_h) / np.cumsum(np.ones_like(all_h)), 'k', label=r'FAR')
plt.plot(plt.gca().get_xlim(), [0.05, 0.05], 'k', linestyle=':', label='0.05')
plt.title('FAR for an increasing number of simulations')
plt.xlabel('Number of simulations')
plt.ylabel('FAR')
plt.legend()
