from scipy.stats import f_oneway, ttest_rel
import matplotlib.pyplot as plt
import numpy as np

from arostest import arostest

np.random.seed(42)

n_obs = 50
A = np.random.normal(size=n_obs)  # normal distributed data (for comparability)

signal = [0.2, 0, 0.1]  # difference in means
noise = 0.5  # uniform noise with U(-noise, noise)

# Construction of data matrix
A = np.asarray([A + signal[0] + np.random.random(n_obs) * 2 * noise - noise,
                A + signal[1] + np.random.random(n_obs) * 2 * noise - noise,
                A + signal[2] + np.random.random(n_obs) * 2 * noise - noise]).T

# obtain aros test p-value
aros = arostest(A, normalize_ranks=False)
p_aros = aros['p']
s_aros = aros['shape']

# obtain pairwise t-test p-values
p_tt_0vs1 = ttest_rel(A[:, 0], A[:, 1])[1]
p_tt_0vs2 = ttest_rel(A[:, 0], A[:, 2])[1]
p_tt_1vs2 = ttest_rel(A[:, 1], A[:, 2])[1]

# obtain one way ANOVA p-value
p_anova = f_oneway(A[:, 0], A[:, 1], A[:, 2])[1]

plt.boxplot(A)
plt.xlabel('group')
plt.ylabel('value')
plt.title('Simulated raw data for %d observations over %d groups' % (n_obs, 3))
