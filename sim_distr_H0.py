import matplotlib.pyplot as plt
import numpy as np

from arostest import arostest

np.random.seed(42)  # for reproducibility

for D, data_dtype, linestyle in zip([
    np.random.normal(size=(50, 3)),
    np.random.random((50, 3)),
    np.random.binomial(3, 0.6, size=(50, 3)),
    np.random.poisson(3, size=(50, 3))],
        ['normal', 'uniform', 'binomial', 'poisson'],
        ['-', '--', '-.', ':']):
    dist = arostest(D, permutations=20000)['distribution']
    # for H0
    dist -= np.mean(dist)
    dist /= np.std(dist)

    # obtain ideal binning (i.e. number of bins)
    counts, bins = np.histogram(dist, bins='auto')
    bin_center = bins[:-1] / 2 + bins[1:] / 2
    dist_height = counts / counts.sum()

    plt.plot(bin_center, dist_height, 'k',
             linestyle=linestyle, label=data_dtype)

plt.xlabel(r'$\sigma$')
plt.ylabel(r'$P(X = x)$')
plt.xlim([-5, 5])
plt.legend(title='data distribution')
plt.title(r'Distribution under $H_0$')
