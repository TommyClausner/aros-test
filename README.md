# aros-test

This is the publication version of the auto-regressive rank order similarity (aros) test, developed by Tommy Clausner and Stefano Gentili. Please see our publication for an in depth explanation and analysis of the test: https://www.biorxiv.org/content/10.1101/2022.06.15.496113v1

In a nutshell, the aros test is a statistical test for pairwise sampled data of more than two experimental conditions (groups). It auto-regressively relates the raw, interval scaled data to a ordinal scaled rank order of multiple group averages. A permutation procedure is used to estimate the data distribution under H0, to which the initial test statistic is compared. A significant result means, that the groups are not interchangeable and the rank order profile (shape) of the means can be explained better by the data than if it would if the group labels were meaningless.
