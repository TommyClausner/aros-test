from scipy.stats import f_oneway, ttest_rel
import matplotlib.pyplot as plt
import numpy as np

from arostest import arostest


def cummean(x):
    """Calculate cumulative average of a data vector.
    
    :param x: 
        The data vector.
    :return: 
    """
    return np.cumsum(x) / np.cumsum(np.ones_like(x))


np.random.seed(42)

n_obs = 50
n_tests = 1000
signal = [0.2, 0, 0.1]  # difference in means
noise = 0.5  # uniform noise with U(-noise, noise)

# initialize result vectors
hs_aros = []
hs_aros_ks = []
hs_t_uc = []
hs_t_b = []
hs_t_bh = []
hs_anova = []
for n in range(n_tests):
    # create data vector
    A = np.random.normal(size=n_obs)

    # Construction of data matrix
    A = np.asarray([A + signal[0] + np.random.random(n_obs) * 2 * noise - noise,
                    A + signal[1] + np.random.random(n_obs) * 2 * noise - noise,
                    A + signal[2] + np.random.random(n_obs) * 2 * noise - noise]
                   ).T

    # obtain aros test h (if shape does not correspond to true shape h = 0)
    aros = arostest(A, normalize_ranks=False)
    hs_aros.append(aros['h'] if all(aros['shape'] == [3, 1, 2]) else 0)

    aros = arostest(A, normalize_ranks=False, shape=[3, 1, 2])
    hs_aros_ks.append(aros['h'])

    # obtain and combine pairwise t-test h
    combined_p_t = np.asarray([
        ttest_rel(A[:, 0], A[:, 1])[1],
        ttest_rel(A[:, 0], A[:, 2])[1],
        ttest_rel(A[:, 1], A[:, 2])[1]
    ])

    hs_t_uc.append(all(combined_p_t < 0.05))  # uncorrected
    hs_t_b.append(all((combined_p_t * 3) < 0.05))  # Bonferroni
    hs_t_bh.append(all((np.sort(combined_p_t) * [3, 2, 1]) < 0.05))  # Holm

    # obtain one way ANOVA h
    hs_anova.append(f_oneway(A[:, 0], A[:, 1], A[:, 2])[1] < 0.05)

# load precomputed data
# npzfile = np.load('pow_data.npz')
# hs_aros = npzfile['hs_aros']
# hs_aros_ks = npzfile['hs_aros_ks']
# hs_t_uc = npzfile['hs_t_uc']
# hs_t_b = npzfile['hs_t_b']
# hs_t_bh = npzfile['hs_t_bh']
# hs_anova = npzfile['hs_anova']

norm_val = np.mean(hs_t_uc)

plt.plot(cummean(hs_aros) / norm_val, 'k', label=r'$aros$')
plt.plot(cummean(hs_aros_ks) / norm_val, 'k', linestyle=':',
         label=r'$aros^{ks}$')
plt.plot(cummean(hs_t_uc) / norm_val, 'gray', label=r'$t^{UC}$')
plt.plot(cummean(hs_t_b) / norm_val, 'gray', linestyle='--', label=r'$t^{B}$')
plt.plot(cummean(hs_t_bh) / norm_val, 'gray', linestyle='-.',
         label=r'$t^{BH}$')
plt.plot(cummean(hs_anova) / norm_val, 'gray', linestyle=':',
         label=r'$ANOVA$')
plt.plot(plt.gca().get_xlim(), [1, 1], 'g',
         label=r'$\sum_{i=1}^Nt_i^{UC}/N$')
plt.legend()
plt.xlabel('Number of simulations (N)')
plt.ylabel(r'$\frac{test\ power}{average\ power\ t^{UC}}$')
plt.title('Cumulative average of relative test power')
